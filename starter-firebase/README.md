# starter-firebase

## Inicializar firebase

Inicializamos el proyecto en la carpeta raiz.

```
firebase init
```

Ejecutamos al menos una vez el archivo de funciones para crear los archivos JS. en la carpeta: `functions/`

Ejecutamos los comandos:
```
npm i
npm run build
```


## Instalar emuladores

Para instalar los emuladores se debe de instalar el cli de firebase. Se necesita Java y Nodejs para que funcones:

```
npm install -g firebase-tools
```

## Instalar emuladores

Para instalar los emuladores se debe de instalar el cli de firebase. Se necesita Java y Nodejs para que funcones:

```
npm install -g firebase-tools
```

Donde este el archivo `.firebaserc` ejecutamos el siguiente comandos:

```
firebase emulators:start
```


