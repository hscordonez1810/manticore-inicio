import * as admin from 'firebase-admin';
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {SIS_CREADO_NOMBRE} from '../../entidades/abstractos/sis-creado-nombre';
import {SIS_MODIFICADO_NOMBRE} from '../../entidades/abstractos/sis-modificado-nombre';
import {PokemonInterface} from '../../entidades/pokemon.interface';

export const POKEMON_DATOS_PRUEBA: PokemonInterface[] = [
    {
        id: '001',
        nombre: 'Bulbasaur',
        tipo: 'Planta / Veneno',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'tipo'],
    },
    {
        id: '004',
        nombre: 'Charmander',
        tipo: 'Fuego',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'tipo'],
    },
    {
        id: '007',
        nombre: 'Squirtle',
        tipo: 'Agua',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'tipo'],
    }
];
