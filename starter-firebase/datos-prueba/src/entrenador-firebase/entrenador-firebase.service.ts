import {Injectable} from '@nestjs/common';
import {FirebaseService} from '../firebase/firebase.service';
import {EntrenadorInterface} from '../entidades/entrenador.interface';
import {FirebaseUtilService} from '../firebase/util/firebase-util.service';
import {NUMERO_TOTAL_REGISTROS} from '../constantes/numero-total-registros';
import {FirestoreServicioAbstractClass} from '@manticore-labs/firebase-nest';

@Injectable()
export class EntrenadorFirebaseService extends FirestoreServicioAbstractClass <EntrenadorInterface> {

    constructor(
        private readonly _firebaseService: FirebaseService,
        private readonly _firebaseUtilService: FirebaseUtilService,
    ) {
        super(
            'entrenador',
            _firebaseService.firestore,
            _firebaseUtilService,
            NUMERO_TOTAL_REGISTROS,
            [
            ],
            'sisHabilitado'
        );
    }
}
