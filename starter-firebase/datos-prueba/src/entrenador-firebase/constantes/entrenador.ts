import * as admin from 'firebase-admin';
import {EntrenadorInterface} from '../../entidades/entrenador.interface';
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {SIS_CREADO_NOMBRE} from '../../entidades/abstractos/sis-creado-nombre';
import {SIS_MODIFICADO_NOMBRE} from '../../entidades/abstractos/sis-modificado-nombre';

export const ENTRENADOR_DATOS_PRUEBA: EntrenadorInterface[] = [
    {
        id: '001',
        nombre: 'Ash Ketchup',
        pueblo: 'Paleta',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'pueblo'],
    },
    {
        id: '002',
        nombre: 'Cristian Lara',
        pueblo: 'Quito',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'pueblo'],
    },
    {
        id: '003',
        nombre: 'Andrés Durán',
        pueblo: 'Ibarra',
        sisHabilitado: ActivoInactivo.Activo,
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        __arregloCamposBusqueda: ['nombre', 'pueblo'],
    }
];
