import {Injectable} from '@nestjs/common';
import {FirebaseService} from '../firebase/firebase.service';
import {FirebaseUtilService} from '../firebase/util/firebase-util.service';
import {NUMERO_TOTAL_REGISTROS} from '../constantes/numero-total-registros';
import {UsuarioInterface} from '../entidades/usuario.interface';
import {FirestoreServicioAbstractClass} from '@manticore-labs/firebase-nest';

@Injectable()
export class UsuarioFirebaseService extends FirestoreServicioAbstractClass <UsuarioInterface> {

    constructor(
        private readonly _firebaseService: FirebaseService,
        private readonly _firebaseUtilService: FirebaseUtilService,
    ) {
        super(
            'usuario',
            _firebaseService.firestore,
            _firebaseUtilService,
            NUMERO_TOTAL_REGISTROS,
            [],
            'sisHabilitado'
        );
    }
}
