import * as admin from 'firebase-admin';
import {SIS_CREADO_NOMBRE} from '../../entidades/abstractos/sis-creado-nombre';
import {SIS_MODIFICADO_NOMBRE} from '../../entidades/abstractos/sis-modificado-nombre';
import {UsuarioInterface} from '../../entidades/usuario.interface';

export const USUARIO_DATOS_PRUEBA: UsuarioInterface[] = [
    {
        password: 'vadrian.eguez@gmail.com',
        email: 'vadrian.eguez@gmail.com',
        id: 'vadrian.eguez@gmail.com',
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        ___camposExtra: {
            habilitado: true,
            nombres: 'Vicente Adrian',
            apellidos: 'Eguez Sarzosa',
            cedulaOPasaporte: '1718137159',
            permisosExtra: ['editarPokemon'],
            objetoDentroDeUsuario: {
                pokemonFavorito: 'Pikachu'
            }
        },
    },
    {
        id: 'andres.david708@gmail.com',
        password: 'andres.david708@gmail.com',
        email: 'andres.david708@gmail.com',
        roles: ['superadmin', 'admin'],
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        ___camposExtra: {
            habilitado: true,
            nombres: 'Oscar Eduardo',
            apellidos: 'Sambache Llumiquinga',
            cedulaOPasaporte: '1214422332',
            permisosExtra: ['verPokemon'],
            objetoDentroDeUsuario: {
                pokemonFavorito: 'Snorlax'
            }
        },
    },
    {
        id: 'cristian89lara@gmail.com',
        password: 'cristian89lara@gmail.com',
        email: 'cristian89lara@gmail.com',
        roles: ['superadmin', 'admin'],
        [SIS_CREADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        [SIS_MODIFICADO_NOMBRE]: admin.firestore.FieldValue.serverTimestamp(),
        ___camposExtra: {
            habilitado: true,
            nombres: 'Cristian Raul',
            apellidos: 'Lara Balarezo',
            cedulaOPasaporte: '1717465569',
            permisosExtra: ['verPokemon'],
            objetoDentroDeUsuario: {
                pokemonFavorito: 'Snorlax'
            }
        },
    }
]
