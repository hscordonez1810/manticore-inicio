import {USUARIO_DATOS_PRUEBA} from '../usuario-firebase/constantes/usuario';
import {ENTRENADOR_DATOS_PRUEBA} from '../entrenador-firebase/constantes/entrenador';

export const DATOS_PRUEBA = {
    usuarios: USUARIO_DATOS_PRUEBA,
    entrenado: ENTRENADOR_DATOS_PRUEBA,
}
