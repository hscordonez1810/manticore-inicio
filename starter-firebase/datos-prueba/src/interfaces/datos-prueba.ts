import {EntrenadorInterface} from '../entidades/entrenador.interface';
import {UsuarioInterface} from '../entidades/usuario.interface';
import {PokemonInterface} from '../entidades/pokemon.interface';

export interface DatosPrueba {
    usuario?: UsuarioInterface[];
    entrenador?: EntrenadorInterface[];
    pokemon?: PokemonInterface[];
}
