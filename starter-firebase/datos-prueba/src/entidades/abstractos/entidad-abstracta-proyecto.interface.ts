import {FieldValue, Timestamp} from '@firebase/firestore-types';
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {EntidadGeneralInterface} from '@manticore-labs/firebase-nest';

export interface EntidadAbstractaProyectoInterface extends EntidadGeneralInterface {
    createdAt?: Timestamp | number | FieldValue;
    updatedAt?: Timestamp | number | FieldValue;
    sisHabilitado?: ActivoInactivo;
}
