import {EntidadAbstractaProyectoInterface} from './abstractos/entidad-abstracta-proyecto.interface';

export interface PokemonInterface extends EntidadAbstractaProyectoInterface {
    nombre?: string;
    tipo?: string;
}
