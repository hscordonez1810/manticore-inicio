import {EntidadAbstractaProyectoInterface} from './abstractos/entidad-abstracta-proyecto.interface';

export interface EntrenadorInterface extends EntidadAbstractaProyectoInterface {
    nombre?: string;
    pueblo?: string;
}
