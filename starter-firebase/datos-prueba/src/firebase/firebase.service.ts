import {Injectable} from '@nestjs/common';
import * as admin from 'firebase-admin';
import {app} from 'firebase-admin/lib/firebase-namespace-api';
import {CONFIG} from '../environment/config';

const llaveFirebase: any = JSON.parse(CONFIG.firebaseLlave);

@Injectable()
export class FirebaseService {
    firebaseAdmin: app.App;
    admin = admin;
    firestore;

    constructor() {
        this.firebaseAdmin = admin.initializeApp({
            credential: admin.credential.cert(llaveFirebase),
        });
        this.firestore = this.firebaseAdmin.firestore();
        const settings = {timestampsInSnapshots: true};
        this.firestore.settings(settings);
    }
}
