import {Module} from '@nestjs/common';
import {FirestoreModule} from './firebase/firestore.module';
import {FirestoreAuthService} from './firebase/auth/firestore-auth.service';
import {CONFIG} from './environment/config';
import {PROVIDERS} from './imports/servicios';
import {DatosPrueba} from './interfaces/datos-prueba';
import {USUARIO_DATOS_PRUEBA} from './usuario-firebase/constantes/usuario';
import {EntrenadorFirebaseService} from './entrenador-firebase/entrenador-firebase.service';
import {ENTRENADOR_DATOS_PRUEBA} from './entrenador-firebase/constantes/entrenador';
import {POKEMON_DATOS_PRUEBA} from './pokemon-firebase/constantes/pokemon';
import {PokemonFirebaseService} from './pokemon-firebase/pokemon-firebase.service';
import {PokemonInterface} from './entidades/pokemon.interface';
import {EntrenadorInterface} from './entidades/entrenador.interface';

@Module({
    imports: [
        FirestoreModule
    ],
    controllers: [],
    providers: [
        ...PROVIDERS
    ],
})
export class AppModule {

    datosPrueba: DatosPrueba = {};

    constructor(
        private readonly _firestoreAuthService: FirestoreAuthService,
        private readonly _entrenadorFirebaseService: EntrenadorFirebaseService,
        private readonly _pokemonFirebaseService: PokemonFirebaseService,
    ) {
        if (CONFIG.datosPrueba.crear) {
            this.crearDatosPrueba().then(() => {
                console.info({
                    mensaje: 'Termino de crear datos',
                });
                console.info({
                    mensaje:
                        'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
                });
            })
                .catch((error) => {
                    console.info({
                        mensaje:
                            'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
                    });
                    console.error({
                        mensaje: 'Hubo errores.',
                        error,
                    });
                });
        } else {
            console.info({
                mensaje:
                    'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
            });
        }
    }

    private async crearDatosPrueba() {
        if (CONFIG.datosPrueba.usuario) {
            await this.crearUsuario01();
        }
        if (CONFIG.datosPrueba.entrenador) {
            await this.crearEntrenador11();
        }
    }

    private async crearUsuario01() {
        this.datosPrueba.usuario = [];
        for (const datoPrueba of USUARIO_DATOS_PRUEBA) {
            let roles = [];
            let camposExtra: any = {};
            if (datoPrueba.roles) {
                roles = [...datoPrueba.roles];
            }
            if (datoPrueba.___camposExtra) {
                camposExtra = {
                    ...datoPrueba.___camposExtra
                }
            }
            const respuesta = await this._firestoreAuthService.crearUseryUsuario(
                {
                    email: datoPrueba.email,
                    password: datoPrueba.password,
                },
                roles,
                camposExtra
            );
            this.datosPrueba.usuario.push(respuesta.usuarioEntidad);
        }
    }

    private async crearEntrenador11() {
        if (CONFIG.datosPrueba.entrenador) {
            this.datosPrueba.entrenador = [];
            for (const datoPrueba of ENTRENADOR_DATOS_PRUEBA) {
                const datoPruebaClonado = {
                    ...datoPrueba
                };
                delete datoPruebaClonado['id'];
                delete datoPruebaClonado['__arregloCamposBusqueda'];
                delete datoPruebaClonado['_colecciones'];
                const respuesta = await this._entrenadorFirebaseService.crear(
                    datoPruebaClonado,
                    datoPrueba.id,
                    false,
                    datoPrueba.__arregloCamposBusqueda,
                    []
                ) as EntrenadorInterface;
                this.datosPrueba.entrenador.push(respuesta);
            }
            await this.crearPokemon21();
        }
    }

    private async crearPokemon21() {
        if (CONFIG.datosPrueba.pokemon) {
            this.datosPrueba.pokemon = [];
            for (let entrenador of this.datosPrueba.entrenador) {
                const arregloIdColeccionesSuperiores = [entrenador.uid];
                for (const datoPrueba of POKEMON_DATOS_PRUEBA) {
                    const datoPruebaClonado = {
                        ...datoPrueba
                    };
                    delete datoPruebaClonado['id'];
                    delete datoPruebaClonado['__arregloCamposBusqueda'];
                    delete datoPruebaClonado['_colecciones'];
                    const respuesta = await this._pokemonFirebaseService.crear(
                        datoPruebaClonado,
                        undefined,
                        false,
                        datoPrueba.__arregloCamposBusqueda,
                        [...arregloIdColeccionesSuperiores]
                    ) as PokemonInterface;
                    this.datosPrueba.pokemon.push(respuesta);
                }
            }
        }
    }

    // private async crearDatosPrueba() {
    //     if (ENVIRONMENT.crearUsuarios) {
    //         for (const usuario of DATOS_PRUEBA.usuarios) {
    //             let roles = [];
    //             let camposExtra: any = {};
    //             if (usuario.roles) {
    //                 roles = [...usuario.roles];
    //             }
    //             if (usuario.___camposExtra) {
    //                 camposExtra = {
    //                     ...usuario.___camposExtra
    //                 }
    //             }
    //             await this._firestoreAuthService.crearUseryUsuario(
    //                 {
    //                     email: usuario.email,
    //                     password: usuario.password,
    //                 },
    //                 roles,
    //                 camposExtra
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearIndicador) {
    //         const indicadores: any = DATOS_PRUEBA.indicador;
    //         for (const indicador of indicadores) {
    //             const indicadorSinId = {
    //                 ...indicador
    //             };
    //             delete indicadorSinId['id'];
    //             await this._firestoreIndicadorService.createOne(
    //                 indicadorSinId,
    //                 indicador.id
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearTipoIndicador) {
    //         const tiposIndicadores: any = DATOS_PRUEBA.tiposIndicador;
    //         for (const tiposIndicador of tiposIndicadores) {
    //             const tiposIndicadorSinId = {
    //                 ...tiposIndicador
    //             };
    //             delete tiposIndicadorSinId['id'];
    //             await this._firestoreTipoIndicadorService.createOne(tiposIndicadorSinId, tiposIndicador.id);
    //         }
    //     }
    //     if (ENVIRONMENT.crearTransporte) {
    //         const transportes: any = DATOS_PRUEBA.transporte;
    //         for (const transporte of transportes) {
    //             const transporteSinId = {
    //                 ...transporte
    //             };
    //             delete transporteSinId['id'];
    //             await this._firestoreTransporteService.createOne(transporteSinId, transporte.id);
    //         }
    //     }
    //     if (ENVIRONMENT.crearCooperativa) {
    //         const cooperativas: any = DATOS_PRUEBA.cooperativa;
    //         for (const cooperativa of cooperativas) {
    //             const cooperativaSinId = {
    //                 ...cooperativa
    //             };
    //             delete cooperativaSinId['id'];
    //             delete cooperativaSinId['__arregloCamposBusqueda'];
    //             await this._firestoreCooperativaService.createOne(
    //                 cooperativaSinId,
    //                 cooperativa.id,
    //                 false,
    //                 cooperativa.__arregloCamposBusqueda
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearRuta) {
    //         const rutas: any = DATOS_PRUEBA.rutas;
    //         for (const ruta of rutas) {
    //             const rutaSinId = {
    //                 ...ruta
    //             };
    //             delete rutaSinId['id'];
    //             delete rutaSinId['_colecciones'];
    //             await this._firestoreRutaService.createOne(
    //                 rutaSinId,
    //                 ruta.id,
    //                 false,
    //                 [],
    //                 ruta._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearPlanOperacional) {
    //         const planOperacionales: any = DATOS_PRUEBA.planOperacionalCooperativa;
    //         for (const planOperacional of planOperacionales) {
    //             const planOperacionalSinId = {
    //                 ...planOperacional
    //             };
    //             delete planOperacionalSinId['id'];
    //             delete planOperacionalSinId['_colecciones'];
    //             await this._firestorePlanOperacionCooperativaService.createOne(
    //                 planOperacionalSinId,
    //                 planOperacional.id,
    //                 false,
    //                 [],
    //                 planOperacional._colecciones,
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearPlanOperacionalHorario) {
    //         const planOperacionalHorarios: any = DATOS_PRUEBA.planOperacionalHorario;
    //         for (const planOperacionalHorario of planOperacionalHorarios) {
    //             const planOperacionalHorarioClon = Object.assign({}, planOperacionalHorario);
    //             delete planOperacionalHorarioClon['_colecciones'];
    //             await this._firestorePlanOperacionHorarioService.createOne(
    //                 planOperacionalHorarioClon,
    //                 undefined,
    //                 false,
    //                 [],
    //                 planOperacionalHorario._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearConfiguracionPuntoControl) {
    //         const configuracionesPuntoControl: any = DATOS_PRUEBA.configuracionPuntoControl;
    //         for (const configuracionPuntoControl of configuracionesPuntoControl) {
    //             const configuracionPuntoControlClon = {...configuracionPuntoControl};
    //             delete configuracionPuntoControlClon['_colecciones'];
    //             delete configuracionPuntoControlClon['id'];
    //             await this._firestoreConfiguracionPuntoControlService.createOne(
    //                 configuracionPuntoControlClon,
    //                 configuracionPuntoControl.id,
    //                 false,
    //                 [],
    //                 configuracionPuntoControl._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearChofer) {
    //         const choferes: any = DATOS_PRUEBA.chofer;
    //         for (const chofer of choferes) {
    //             const choferClon = {...chofer};
    //             delete choferClon['_colecciones'];
    //             delete choferClon['id'];
    //             await this._firestoreChoferService.createOne(
    //                 choferClon,
    //                 chofer.id,
    //                 false,
    //                 [],
    //                 chofer._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearAyudante) {
    //         const ayudantes: any = DATOS_PRUEBA.ayudante;
    //         for (const ayudante of ayudantes) {
    //             const ayudanteClon = {...ayudante};
    //             delete ayudanteClon['_colecciones'];
    //             delete ayudanteClon['__arregloCamposBusqueda'];
    //             delete ayudanteClon['id'];
    //             const anteriorRegistro = await this._firestoreAyudanteService.createOne(
    //                 ayudanteClon,
    //                 ayudante.id,
    //                 false,
    //                 ayudante.__arregloCamposBusqueda,
    //                 ayudante._colecciones
    //             );
    //             await this._firestoreAyudanteService.updateOne(
    //                 anteriorRegistro,
    //                 {
    //                     usuario: {
    //                         nombres: 'OTRO CHOCI',
    //                         apellidos: 'MIN LAU',
    //                         cedulaOPasaporte: '1718137122',
    //                         uid: '33.3@gmail.com'
    //                     }
    //                 },
    //                 ayudante._colecciones
    //             )
    //         }
    //     }
    //
    //     if (ENVIRONMENT.crearPuntoControl) {
    //         const puntosControl: any = DATOS_PRUEBA.puntosControl;
    //         for (const puntoControl of puntosControl) {
    //             const puntoControlClon = {...puntoControl};
    //             delete puntoControlClon['_colecciones'];
    //             delete puntoControlClon['id'];
    //             await this._firestorePuntoControlService.createOne(
    //                 puntoControlClon,
    //                 puntoControl.id,
    //                 false,
    //                 [],
    //                 puntoControl._colecciones
    //             );
    //         }
    //     }
    //
    //     if (ENVIRONMENT.crearRutaTransporteDiario) {
    //         const rutasTransporteDiario: any = DATOS_PRUEBA.rutaTransporteDiario;
    //         for (const rutaTransporteDiario of rutasTransporteDiario) {
    //             const rutaTransporteDiarioClon = {...rutaTransporteDiario};
    //             delete rutaTransporteDiarioClon['_colecciones'];
    //             await this._firestoreRutaTransporteDiarioService.createOne(
    //                 rutaTransporteDiarioClon,
    //                 undefined,
    //                 false,
    //                 [],
    //                 rutaTransporteDiario._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearPuntoControlAlcanzado) {
    //         const puntosControlAlcanzados: any = DATOS_PRUEBA.puntoControlAlcanzado;
    //         for (const puntoControlAlcanzado of puntosControlAlcanzados) {
    //             const puntoControlAlcanzadoClon = {...puntoControlAlcanzado};
    //             delete puntoControlAlcanzadoClon['_colecciones'];
    //             await this._firestorePuntoControlAlcanzadoService.createOne(
    //                 puntoControlAlcanzadoClon,
    //                 undefined,
    //                 false,
    //                 [],
    //                 puntoControlAlcanzado._colecciones
    //             );
    //         }
    //     }
    //     if (ENVIRONMENT.crearGeolocalizacionRutaTransporteDiario) {
    //         const geolocalizacionesRutaTransporteDiarios: any = DATOS_PRUEBA.puntoControlAlcanzado;
    //         for (const geolocalizacionRutaTransporteDiario of geolocalizacionesRutaTransporteDiarios) {
    //             const geolocalizacionRutaTransporteDiarioClon = {...geolocalizacionRutaTransporteDiario};
    //             delete geolocalizacionRutaTransporteDiarioClon['_colecciones'];
    //             await this._firestoreGeolocalizacionRutaTransporteDiarioService.createOne(
    //                 geolocalizacionRutaTransporteDiarioClon,
    //                 undefined,
    //                 false,
    //                 [],
    //                 geolocalizacionRutaTransporteDiario._colecciones
    //             );
    //         }
    //     }
    //
    //
    // }
}
