import {EntrenadorFirebaseService} from '../entrenador-firebase/entrenador-firebase.service';
import {PokemonFirebaseService} from '../pokemon-firebase/pokemon-firebase.service';

export const PROVIDERS = [
    EntrenadorFirebaseService,
    // UsuarioFirebaseService, // NO IMPORTAR AL USUARIO
    PokemonFirebaseService,
];
