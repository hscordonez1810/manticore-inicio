export const CONFIG = {
    datosPrueba: {
        crear: true,
        usuario: true,
        entrenador: true,
        pokemon: true,
    },
    firebaseLlave: '{\n' +
        '  "type": "service_account",\n' +
        '  "project_id": "sos-encargos-test",\n' +
        '  "private_key_id": "6cb5ebd67ac358fe5e019542b9f3c93947ef9d7d",\n' +
        '  "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC3qGXr6XsNgGL4\\no4KgneeyCMcRJ21zPKircngP3C49VBdg24lDR8/OfA+XurSqvb/g3Jkn0LvIN+Oi\\nVaqLgQXVxR7xZoCmqvACguMvtc3pYw61VvL+Tpqq5GFZaBf27EJV/YghS7HE3SwP\\nRvoKI1HODmTVyfalkb/G+8lzPWmB8HKkoYBIezvkKmI2D7Yq/pnGOpZnOHdVEr1M\\nuaMmZBIduLiadtHiEYjx2k86YUx/N8ry3oyj/FqLlt9J3G3jFuNrMLBoaRbFzJSq\\na4dUGe0JTIKe3y6Qi+CJkEobJevYavfqbdtYDuzZF56kzgqK0lL6UoalWpr5LWMq\\nFlbZcMABAgMBAAECggEAAI3STGJQeeWDLuox2b2OMk0Y2iA1Ah/qx5UdX/lfXZWe\\nSi9RWD3MRnx88M2KL9lpjbJmpx5H4bpK7k19OaVgjND2114yeUY62fwWdrlw+wEu\\nrTz7V8lpiaZNlZ8k8tyKtO8SSPztJumOoWdqt8r8Wz9zfgpZW02fryhJZtbGpaLg\\na8QR9yXJnYPV8WExXbp11plvYG9ull/kx5Ano9UIhBYONBWZorPFTZQiScHzBUh/\\n2us9NYdTKZ9UfdOlND6hqvSDQAsOh+CWF1bx3x0WtWfYYZ+GSntXhf7dTpspHpmW\\n0XzV50NG0mWsMMpiNz9IdMJCXQeVVcOHpy4hvOb5QQKBgQDZxlFVnRHie8sg04YS\\nZHUUL39p/EyRkhirYmZ0O1wnWXRcMb8x8+rvPZS0PsbDlXFf1ejrbdc+gS1RyvC4\\nejR8tk3DTw3XOzFPMvM++/O5dZLbxap/3Yj+q2zPxJGDoHBN19o1sjZmu4bY0GKu\\n8Wfa2+W2vtgErgi3KtO9Bfyd4QKBgQDX5QrUMN7uqo9ixnrAxYxErpO1NImcrywM\\n9AqQTPtTbIVh0AC/q5S/Sqa9Mn6TS65xsZj0T0WkFMgZoq3VcN37b3Pb8wAEyVeR\\npGxvcQxKFaNtbMrt+5m9ag9WcNhDTFj6v0GjaoePMrJozR60DfcIxyPkfR6lD1hB\\nNqf1KJEmIQKBgDRIboJ1YTPUsKHC5q9HuuwLszVIw0E3qACNcTd6gNdZnJv9vX9K\\nph6l1Cup3qJaIIv3mSHcjw6MiMrYRA6mGC/QvL6lHheMOHxhKRD4AD2IQqfMvvBP\\nyXIKyebftGe7fvrRVawKixEC+I7ZCLgjcOGUZYQIMWh+N81x1L/OziVBAoGBAIO/\\n+L3JHSLU3y/OI/ckFxeK2gUJDLmbhOxauV+aOv2MClHIl8xgLyQVsG9hMzE8TDaB\\nxO/CuKMLM5nBVwgcGrtoJnIhdu9014W/q5TOjktfrR4H+EcG9alL4hRERRW8r8hF\\nkw7g4d0XeRzPoF8M7qZisJqtaFgeZyNU8WaA3SNBAoGBAJDlqAta/KQgCQo4wa2p\\n/eT6HwZLSmnufRKsjSBUX1QtYXDxm5V6vJHfYs6KqSkiDWG2qo8nP2pwHBMBtQry\\nyIHFATCbWc8yHZaeD1Lw+2zaTdGPRxmP4/S4igarZMrPg2qbl/Zr6G8P8998Koa3\\nX5RDIy5icDkLpttZvX/XxkP5\\n-----END PRIVATE KEY-----\\n",\n' +
        '  "client_email": "firebase-adminsdk-1jmrh@sos-encargos-test.iam.gserviceaccount.com",\n' +
        '  "client_id": "109063786291849245843",\n' +
        '  "auth_uri": "https://accounts.google.com/o/oauth2/auth",\n' +
        '  "token_uri": "https://oauth2.googleapis.com/token",\n' +
        '  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",\n' +
        '  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-1jmrh%40sos-encargos-test.iam.gserviceaccount.com"\n' +
        '}',
}
