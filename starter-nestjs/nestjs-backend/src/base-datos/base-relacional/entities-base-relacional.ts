
import { MusicaEntity } from '../../musica/musica.entity';
import { ProyectEntity } from '../../proyect/proyect.entity';
import {EntrenadorEntity} from '../../entrenador/entrenador.entity';
import {PokemonEntity} from '../../pokemon/pokemon.entity';

export const ENTITIES_BASE_RELACIONAL = [
    EntrenadorEntity,
    PokemonEntity,
    ProyectEntity,
    MusicaEntity
];
