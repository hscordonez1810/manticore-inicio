import {CONFIG} from '../../environment/config';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ENTITIES_NO_RELACIONAL} from './entities-no-relacional';

export const CONEXION_BASE_NO_RELACIONAL = TypeOrmModule.forRoot({
    name: CONFIG.mongodb.MONGO_NOMBRE_CONEXION,
    type: CONFIG.mongodb.MONGO_TYPE as 'mongodb',
    authSource: "admin",
    useNewUrlParser: CONFIG.mongodb.MONGO_USE_NEW_URL_PARSER,
    useUnifiedTopology: true,
    dropSchema: CONFIG.mongodb.MONGO_DROP_SCHEMA,
    url: CONFIG.mongodb.MONGO_URL,
    entities: [...ENTITIES_NO_RELACIONAL],
});


