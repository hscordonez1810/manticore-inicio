import {CONEXION_BASE_RELACIONAL} from '../base-datos/base-relacional/conexion-base-relacional';
import {CONEXION_BASE_AUDITORIA} from '../base-datos/base-auditoria/conexion-base-auditoria';
import {CONEXION_BASE_SEGURIDAD} from '../base-datos/base-seguridad/conexion-base-seguridad';
import {CONEXION_BASE_NO_RELACIONAL} from '../base-datos/base-no-relacional/conexion-base-no-relacional';
import {EntrenadorModule} from '../entrenador/entrenador.module';
import {PokemonModule} from '../pokemon/pokemon.module';
import {ProyectModule} from '../proyect/proyect.module';
import {MusicaModule} from '../musica/musica.module';


export const IMPORTS = [
    // FirestoreModule, // Si se desea trabajar con el modulo de firebase des-comentar esto
    CONEXION_BASE_RELACIONAL,
    CONEXION_BASE_AUDITORIA,
    CONEXION_BASE_SEGURIDAD,
    CONEXION_BASE_NO_RELACIONAL,
    EntrenadorModule,
    PokemonModule,
    ProyectModule,
    MusicaModule,
];
