import { Module } from '@nestjs/common';
import { MusicaController } from './musica.controller';
import { MUSICA_IMPORTS } from './constantes/musica.imports';
import { MUSICA_PROVIDERS } from './constantes/musica.providers';

@Module({
  imports: [...MUSICA_IMPORTS],
  providers: [...MUSICA_PROVIDERS],
  exports: [...MUSICA_PROVIDERS],
  controllers: [MusicaController],
})
export class MusicaModule {}
