import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class MusicaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
  @IsOptional()
  @IsString()
  @MinLength(2)
  @MaxLength(60)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @Expose()
  album: string;

  @IsOptional()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @Expose()
  anio: string;

}
