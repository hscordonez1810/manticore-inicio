import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import { HabilitadoDtoComun } from 'src/abstractos/habilitado-dto-comun';

export class MusicaCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;
    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(100)
    @Expose()
    album: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(100)
    @Expose()
    anio: string;

   /*  @IsNotEmpty()
    @IsNumber()
    @Expose()
    proyect: number;  */
}
