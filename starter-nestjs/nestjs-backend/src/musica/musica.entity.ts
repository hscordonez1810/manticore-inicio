import { EntidadComunProyecto } from 'src/abstractos/entidad-comun-proyecto';
import { PREFIJO_BASE } from 'src/constantes/prefijo-base';
import { ProyectEntity } from 'src/proyect/proyect.entity';
import {
    Column,
    Entity,
    Index,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';
const PREFIJO_TABLA = ''; // EJ: PREFIJO_
const nombreCampoNombre = PREFIJO_TABLA + 'NOMBRE';
const nombreCampoAlbum = PREFIJO_TABLA + 'ALBUM';
const nombreCampoAno = PREFIJO_TABLA + 'ANIO';
@Entity(PREFIJO_BASE + 'MUSICA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class MusicaEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_MUSICA',
        unsigned: true,
    })
    id: number;

    @Column({
        name: nombreCampoNombre,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    nombre: string;

    @Column({
        name: nombreCampoAlbum,
        type: 'varchar',
        length: '100',
        nullable: false,
    })
    album: string;

    @Column({
        name: nombreCampoAno,
        type: 'char',
        length: '100',
        nullable: false,
    })
    anio: string;

    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];


    //descomentar y ver error en    la relacion

  /*   @ManyToOne(() => ProyectEntity, (r) => r.musica, { nullable: false })
    proyecto: ProyectEntity; */
}
