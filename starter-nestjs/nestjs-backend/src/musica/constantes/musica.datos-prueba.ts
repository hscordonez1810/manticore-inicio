import { ActivoInactivo } from 'src/enums/activo-inactivo';
import { MusicaCrearDto } from '../dto/musica.crear.dto';

export const MUSICA_DATOS_PRUEBA: (() => MusicaCrearDto)[] = [
  () => {
    const dato = new MusicaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Crimen';
    dato.album = 'Crimen';
    dato.anio = '2020';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
 
];
