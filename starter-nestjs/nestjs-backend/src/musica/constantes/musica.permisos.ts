import { ObjetoSeguridadPermisos } from "@manticore-labs/nest-2021";

export const PERMISOS_MUSICA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'musica';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/musica/, // /musica
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/musica\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /musica?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/musica\/\d+\/modificar-habilitado/, // /musica/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/musica\/\d+/, // /musica/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
