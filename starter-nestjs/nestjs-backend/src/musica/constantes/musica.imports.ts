import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditoriaModule } from 'src/auditoria/auditoria.module';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';
import { SeguridadModule } from 'src/seguridad/seguridad.module';
import { MusicaEntity } from '../musica.entity';

export const MUSICA_IMPORTS = [
  TypeOrmModule.forFeature([MusicaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
