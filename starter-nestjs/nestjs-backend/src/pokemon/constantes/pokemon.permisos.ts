import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_POKEMON: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'pokemon';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/pokemon/, // /pokemon
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/pokemon\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /pokemon?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/pokemon\/\d+\/modificar-habilitado/, // /pokemon/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/pokemon\/\d+/, // /pokemon/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
