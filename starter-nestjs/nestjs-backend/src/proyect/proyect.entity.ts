import {
    Column,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { EntidadComunProyecto } from '../abstractos/entidad-comun-proyecto';
import { PREFIJO_BASE } from '../constantes/prefijo-base';
import { MusicaEntity } from '../musica/musica.entity';
// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

const PREFIJO_TABLA = ''; // EJ: PREFIJO_
const nombreCampoNombre = PREFIJO_TABLA + 'NOMBRE';
const nombreCampoGenero = PREFIJO_TABLA + 'GENERO';
const nombreCampoPais = PREFIJO_TABLA + 'PAIS';

@Entity(PREFIJO_BASE + 'PROYECT')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ProyectEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_PROYECT',
        unsigned: true,
    })
    id: number;

    @Column({
        name: nombreCampoNombre,
        type: 'varchar',
        length: '200',
        nullable: false,
    })
    nombre: string;

    @Column({
        name: nombreCampoGenero,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    genero: string;

    @Column({
        name: nombreCampoPais,
        type: 'char',
        length: '20',
        nullable: false,
    })
    pais: string;

    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];

  /*   @OneToMany(() => MusicaEntity, (r) => r.proyecto)
    musica: MusicaEntity[]; */

    /* @OneToMany(() => PokemonEntity, (r) => r.entrenador)
    pokemons: PokemonEntity[]; */
}
