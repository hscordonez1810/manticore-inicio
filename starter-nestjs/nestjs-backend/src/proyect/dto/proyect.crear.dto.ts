import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import { HabilitadoDtoComun } from '../../abstractos/habilitado-dto-comun';

export class ProyectCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(60)
    @Expose()
    nombre: string;
  
    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(60)
    @Expose()
    genero: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(60)
    @Expose()
    pais: string;
}
