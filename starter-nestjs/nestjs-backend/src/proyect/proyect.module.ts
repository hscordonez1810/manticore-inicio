import { Module } from '@nestjs/common';
import { ProyectController } from './proyect.controller';
import { PROYECT_IMPORTS } from './constantes/proyect.imports';
import { PROYECT_PROVIDERS } from './constantes/proyect.providers';

@Module({
  imports: [...PROYECT_IMPORTS],
  providers: [...PROYECT_PROVIDERS],
  exports: [...PROYECT_PROVIDERS],
  controllers: [ProyectController],
})
export class ProyectModule {}
