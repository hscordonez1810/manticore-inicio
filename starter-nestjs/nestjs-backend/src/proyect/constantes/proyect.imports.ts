import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditoriaModule } from 'src/auditoria/auditoria.module';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';
import { SeguridadModule } from 'src/seguridad/seguridad.module';
import { ProyectEntity } from '../proyect.entity';

export const PROYECT_IMPORTS = [
  TypeOrmModule.forFeature([ProyectEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
