import { ObjetoSeguridadPermisos } from "@manticore-labs/nest-2021";

export const PERMISOS_PROYECT: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'proyect';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/proyect/, // /proyect
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/proyect\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /proyect?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/proyect\/\d+\/modificar-habilitado/, // /proyect/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/proyect\/\d+/, // /proyect/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
