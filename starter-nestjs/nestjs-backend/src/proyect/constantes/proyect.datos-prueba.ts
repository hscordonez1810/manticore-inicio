import { ActivoInactivo } from 'src/enums/activo-inactivo';
import { ProyectCrearDto } from '../dto/proyect.crear.dto';

export const PROYECT_DATOS_PRUEBA: (() => ProyectCrearDto)[] = [
  () => {
    const dato = new ProyectCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Cerati';
    dato.genero = 'Rock';
    dato.pais = 'Argentina';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new ProyectCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'leon';
    dato.genero = 'Rock alternativo';
    dato.pais = 'Mexico';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
