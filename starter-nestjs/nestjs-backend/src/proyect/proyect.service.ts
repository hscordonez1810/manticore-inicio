import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ProyectEntity } from './proyect.entity';
import {ProyectBusquedaDto} from './dto/proyect.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from 'src/auditoria/auditoria.service';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';

@Injectable()
export class ProyectService extends ServicioComun<ProyectEntity, ProyectBusquedaDto> {
  constructor(
    @InjectRepository(ProyectEntity, NOMBRE_CADENA_CONEXION)
    public proyectEntityRepository: Repository<ProyectEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        proyectEntityRepository,
      _connection,
        ProyectEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
